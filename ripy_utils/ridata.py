import pyfftw
import pickle
from numpy import fft, zeros, real, conj, ones_like


def read_clock(data, bitnum=15):
    d = ((data >> bitnum) & 1).astype('i1')
    counts = sum(abs(d[1:] - d[:-1]))
    return  float(counts) / float(len(d) - 1) * 4.



class RIData(object):
    def __init__(self, navg=200, bw=1000, clockbit=None, dev=None):
        self.rate = 80e6
        self.navg = navg
        self.bw = bw
        self.nft = int(self.rate / self.bw)
        self.dt = 1./self.rate
        self.percapture = 2

        self.lastspectrum = None
        self.lastfactor = 1.
        self.clockbit = clockbit

        print("BW: %f Hz" % self.bw)
        print("T: %f s" % (self.nft / self.rate * self.navg))
        print("NFT: %d" % (self.nft,)) 

        if dev is None:
            import ripy
            self.dev = ripy.Device()
        else:
            self.dev = dev


        try:
            with open("fftw_shit.py.wisdom", 'rb') as f:
                pyfftw.import_wisdom(pickle.load(f))
            print("imported wisdom")
        except IOError:
            # no wisdom file found
            pass

        self.inn = pyfftw.empty_aligned(self.nft, 'float64')
        self.out = pyfftw.empty_aligned(self.nft//2 + 1, 'complex128')
        self.FTer = pyfftw.FFTW(self.inn, self.out, flags=["FFTW_ESTIMATE", "FFTW_DESTROY_INPUT"], threads=4)

        # save fftw wisdom
        with open("fftw_shit.py.wisdom", 'wb') as f:
            pickle.dump(pyfftw.export_wisdom(), f)

    def get_spectrum_yield(self):
        Z = zeros(self.nft//2 + 1)
        self.lastfactor = 0
        self.lastfactor_err = 1. / (self.nft*self.percapture)
        cal = self.dev.get_calibration()

        for i in range(self.navg):
            j = i % self.percapture
            if j == 0:
                #data = self.dev.get_calibrated_data(self.nft*self.percapture)
                data = self.dev.get_raw_data(self.nft*self.percapture, mask=False)
                
                if self.clockbit is not None:
                    self.lastfactor += read_clock(data, bitnum=self.clockbit)*self.percapture
                    
            self.inn[:] = (data[j*self.nft:(j+1)*self.nft] & 0x3fff)*cal[0] + cal[1]
            self.FTer.execute()
            zi = real(self.out * conj(self.out)) / float(1 * self.nft * self.rate) * 2
            zi[0] *= 0.5

            Z += zi / float(self.navg)
            
            yield i

        self.lastfactor *= 1. / float(self.navg)
        self.lastspectrum = Z


    def get_spectrum(self, prints=True):
        for i in self.get_spectrum_yield():
            if prints and i % 10 == 0:
                print(i)

        return self.lastspectrum
        


    def get_fs(self):
        return fft.rfftfreq(self.nft, self.dt)



if __name__ == "__main__":
    rd = RIData()

    z = rd.get_spectrum()
    fs = rd.get_fs()

    import matplotlib.pyplot as pp
    pp.figure()
    mask = ones_like(z, dtype=bool)
    mask[0] = False
    pp.plot(fs[mask], z[mask])
    pp.show(block=True)
    