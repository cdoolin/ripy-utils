import pyfftw
import pickle
import numpy, numpy.fft
from scipy import signal


# used to cache pyfftw wisdom in memory
pyfftw_wisdom = None


def init_rfftw(NFT, wisdom="fftw_py.wisdom", threads=2):
    """ initialize pyfftw and create input and output arrays that are aligned
    and in the right size for a real fft with NFT input values

    Args:
        NFT (int): The number of values to FFT.  The real fft will have NFT//2 + 1 output values
        wisdom (str): Caches the FFTW "wisdom" parameters t a file to speed up sequential computations.
            If None is given, the wisdom is cached in memory but will be lost after the program quits
        threads (int): Number of threads FFTW uses for the FFT

    Returns:
        A 3-tuple containing (input array, output array, FFTW object).
    """

    global pyfftw_wisdom

    if wisdom is not None:
        try:
            with open("fftw_py.wisdom", 'rb') as f:
                pyfftw_wisdom = pickle.load(f)
        except IOError:
            pass

    if pyfftw_wisdom is not None:
        pyfftw.import_wisdom(pyfftw_wisdom)


    inn = pyfftw.empty_aligned(NFT, 'float64')
    out = pyfftw.empty_aligned(NFT//2 + 1, 'complex128')

    FTer = pyfftw.FFTW(inn, out, flags=["FFTW_ESTIMATE", "FFTW_DESTROY_INPUT"], threads=4)

    pyfftw_wisdom = pyfftw.export_wisdom()
    if wisdom is not None:
        with open(wisdom, 'wb') as f:
            pickle.dump(pyfftw_wisdom, f)

    return (inn, out, FTer)





def take_spectrum(ridev, navg, rbw, 
    window='boxcar',
    wisdom="fftw_py.wisdom", verbose=True, threads=2):
    """
    Estimates the Power Spectral Density (PSD) of the current signal incident on
    the ripy Device.  This is done using Bartletts method of PSD estimation,
    averaging successive Fourier Transforms to estimate the spectral density.

    Data is captured from the ripy device using the get_calibrated_data method,
    so the returned PSD, if using the Resolved Instruments DPD80, will be in 
    units of uW^2 / Hz.

    Args:
        ridev (ripy.Device): The ripy Device to capture data from
        navg (int): Number of Fourier Transforms to average together
        rbw (float): The Resolution Bandwidth, ie. spacing between FT bins.
            This will be truncated to the nearest integer number of bins.

    Returns:
        (fs, Z), where fs is the numpy array of frequencies each bin is at,
        and Z, the estimated PSD in uW^2 / Hz.
    """
    
    RATE = ridev.samplerate
    NFT = int(RATE / rbw)

    fs = numpy.fft.rfftfreq(NFT, 1./RATE)
    Z = numpy.zeros(NFT//2 + 1)
    
    norm = 2. / float(navg * NFT * RATE)

    inn, out, FTer = init_rfftw(NFT, wisdom=wisdom, threads=threads)

    if window != "boxcar":
        w = signal.get_window(window, NFT)
        w = w / numpy.mean(w)
    else:
        w = 1

    for i in range(navg):
        inn[:] = ridev.get_calibrated_data(NFT) * w
        FTer.execute()
        Z += numpy.real(out * numpy.conj(out)) * norm

    Z[0] *= 0.5

    return fs, Z


def calc_spectrum(x, navg,  
    window='boxcar',
    wisdom="fftw_py.wisdom", threads=2, fs=1):
    navg = int(navg)
    NFT = len(x) // navg

    fs = numpy.fft.rfftfreq(NFT, 1. / fs)
    Z = numpy.zeros(NFT//2 + 1)
    
    norm = 2. / float(navg * NFT * fs)

    inn, out, FTer = init_rfftw(NFT, wisdom=wisdom, threads=threads)

    if window != "boxcar":
        w = signal.get_window(window, NFT)
        w = w / numpy.mean(w)
    else:
        w = 1

    for i in range(navg):
        inn[:] = x[i * NFT:(i + 1) * NFT] * w
        FTer.execute()
        Z += numpy.real(out * numpy.conj(out)) * norm

    Z[0] *= 0.5

    return fs, Z

