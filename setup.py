import os
from setuptools import setup


def read(fname):
    with open(os.path.join(os.path.dirname(__file__), fname)) as f:
        r = f.read()
    return r



setup(
    name = "ripy-utils",
    version = "0.1.0",
    author = "Callum Doolin",
    author_email = "callum@resolvedinstruments.com",
    description = ("Useful function for use with ripy."),
    packages=['ripy_utils'],
    install_requires = ['pyfftw', 'numpy', 'scipy'],
    long_description=read('README'),
)